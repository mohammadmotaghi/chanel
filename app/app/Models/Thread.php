<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;

class Thread extends Model
{
    public function chanel()
    {

        return $this->belongsTo(Chanel::class);
    }

  public function user()
  {
        return $this->belongsTo(Uses::class);
  }
  public function answers()
  {
        return $this->hasMany(Answer::class);
  }

}
